package com.esime.authjwt.model;

public enum RoleName {
	ROLE_USER,
    ROLE_PM,
    ROLE_ADMIN
}

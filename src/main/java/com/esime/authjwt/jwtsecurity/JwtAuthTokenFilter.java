package com.esime.authjwt.jwtsecurity;
import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.esime.authjwt.jwtsecurity.JwtProvider;
import com.esime.authjwt.services.UserDetailsServiceImpl;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
@Component
public class JwtAuthTokenFilter extends OncePerRequestFilter {
	private static final Logger logger = LoggerFactory.getLogger(JwtAuthTokenFilter.class);
	@Autowired
	private UserDetailsServiceImpl UserDetailsService;
   @Autowired
	private JwtProvider jwtProvider;
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String jwt = getJwt(request);
		try {
			if (jwt != null && jwtProvider.validateJwtToken(jwt)) {
				 String username = jwtProvider.getUserNameFronJwtToken(jwt);
				 UserDetails userDetails = UserDetailsService.loadUserByUsername(username);
				 
				 UsernamePasswordAuthenticationToken authentication =new UsernamePasswordAuthenticationToken(userDetails,null, userDetails.getAuthorities());
				 authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				 
				 SecurityContextHolder.getContext().setAuthentication(authentication); 
				}
		} catch (Exception e) {
			logger.error("Can NOT set user authentication -> Message: {}", e);
		}		
		
		filterChain.doFilter(request, response);	
	}
	
	
	
	private String getJwt(HttpServletRequest request){
		String header = request.getHeader("Authorization");
		
		if (header != null &&  header.startsWith("Baerer")) {
			return header.replace("Baerer", "");
		}
		
		return null;
	}

}
